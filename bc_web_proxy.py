#!/usr/bin/env python3
# Python Network Programming Cookbook -- Chapter - 4
# This program is optimized for Python 2.7.
# It may run on any other version with/without modifications.

import argparse
from http.server import BaseHTTPRequestHandler, HTTPServer

DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 8800

# ************************
class baseDataHandler:
    def __init__(self, conf = {}):
        pass
    def handle(self, data_str):
        pass

class printToScreenDataHandler(baseDataHandler):
    def __init__(self, conf = {}):
        super(printToScreenDataHandler, self).__init__()
    def handle(self, data_str):
        if len(data_str) <= 40: print(data_str)
        else: print(data_str[:17] + ' ... ' + data_str[-18:])

class writeToFileDataHandler(baseDataHandler):
    def __init__(self, conf = {"file": "./httprqst.txt",}):
        self.fn = conf["file"]
        super(writeToFileDataHandler, self).__init__()
    def handle(self, data_str):
        with open(self.fn, 'a+') as f:
            print(data_str, file = f, end = '\n')
# ************************
def checkContentTypeJson(headers):
    if headers['content-type'] == 'application/json': return True
    return False
def checkContentLength(headers, max_len):
    if int(headers['content-length']) <= max_len: return True
    return False
def checkPrintHeaders(headers):
    print(headers)
    return True
# ************************
headerCheck = [ checkContentTypeJson, lambda h: checkContentLength(h, 1024), checkPrintHeaders, ]
dataHandler = [ printToScreenDataHandler(), writeToFileDataHandler(), ]

class RequestHandler(BaseHTTPRequestHandler):
    """ Custom request handler """
    def do_GET(self):
        """ Handler for the GET requests """
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the message to browser
        self.wfile.write('<html><header></header><body></body></html>'.encode())

    def do_POST(self):

        def content_length(headers):
            return int(headers['content-length'])

        if all([hc(self.headers) for hc in headerCheck]):
            content = self.rfile.read( content_length(self.headers) ).decode()
            for dh in dataHandler: dh.handle(content)
            self.send_response(200)
            self.end_headers()
        else:
            self.send_response(500)
            self.end_headers()
  
class CustomHTTPServer(HTTPServer):
    """A custom HTTP server"""
    def __init__(self, host, port):
      server_address = (host, port)
      HTTPServer.__init__(self, server_address, RequestHandler)
  
def run_server(port):
    try:
      server= CustomHTTPServer(DEFAULT_HOST, port)
      print("Custom HTTP server started on port: ", port)
      server.serve_forever()
    except Exception:
      print("Some error occured.")
    except KeyboardInterrupt:
      print("Server interrupted and is shutting down...")
      server.socket.close()
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Simple HTTP Server Example')
    parser.add_argument('--port', action="store", dest="port", type=int, default=DEFAULT_PORT)
    given_args = parser.parse_args() 
    port = given_args.port
    run_server(port)
